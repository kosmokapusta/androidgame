package com.space.framework.implementation;
/**
 * Created by lina on 8/2/13.
 */
import java.util.List;

import android.view.View.OnTouchListener;

import com.space.framework.Input.TouchEvent;

public interface TouchHandler extends OnTouchListener {
    public boolean isTouchDown(int pointer);

    public int getTouchX(int pointer);

    public int getTouchY(int pointer);

    public List<TouchEvent> getTouchEvents();
}
