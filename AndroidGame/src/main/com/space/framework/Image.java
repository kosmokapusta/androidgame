package com.space.framework;

/**
 * Created by lina on 8/1/13.
 */
public interface Image {
    public int getWidth();
    public int getHeight();
    public Graphics.ImageFormat getFormat();
    public void dispose();
}
