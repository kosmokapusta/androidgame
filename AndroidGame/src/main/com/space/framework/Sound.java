package com.space.framework;

/**
 * Created by lina on 8/1/13.
 */
public interface Sound {

    public void play(float volume);

    public void dispose();
}
