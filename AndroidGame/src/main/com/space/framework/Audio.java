package com.space.framework;

/**
 * Created by lina on 8/1/13.
 */
public interface Audio {

    public Music createMusic(String file);

    public Sound createSound(String file);
}
