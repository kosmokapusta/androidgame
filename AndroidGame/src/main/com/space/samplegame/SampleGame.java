package com.space.samplegame;
/**
 * Created by lina on 8/2/13.
 */

import com.space.framework.Screen;
import com.space.framework.implementation.AndroidGame;

public class SampleGame extends AndroidGame {
    @Override
    public Screen getInitScreen() {
        return new LoadingScreen(this);
    }
}
